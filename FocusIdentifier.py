from ImageProcessing import ImageProcessing
from CameraAcquisition import CameraAcquisition
import cv2
import numpy as np
import time
import pyqtgraph as pg
from pyqtgraph.Qt import QtGui
import threading
import platform
import pygame


class FocusIdentifier:
    def __init__(self):
        self.areas = np.array([])
        self.means = np.array([])
        self.stds = np.array([])
        self.events = np.array([])
        self.key_mean = None
        self.start_time_event = 0
        self.th1 = ObjectActionThread('th1', pin=27)
        self.th1.start()

    def FilteredArea(self, area, max_area=-1):
        if area == 0 and len(self.areas) > 1:
            area = self.areas[-1]
        else:
            if max_area != -1:
                if area >= max_area:
                    if len(self.areas) > 0:
                        area = self.areas[-1]
                    else:
                        area = 0
        self.areas = np.append(self.areas, area)
        return area

    def IdentifyFocus(self, fps, thr=0.15):
        n = int(2 * fps)  # dipendente dai fps del sistema
        if len(self.areas) <= n:
            window = self.areas
        else:
            window = self.areas[-n - 1: -2]
        last_area = self.areas[-1]
        win_mean = np.mean(window)
        self.means = np.append(self.means, win_mean)

        # CONDITION FOR AREA VALUES UNDER THRESHOLD WHILE WATCHING NEAR OBJECT
        second_comm = None
        if self.key_mean and last_area < self.key_mean:
            event = 1

            # CONTROLLING IF THERE IS A SECOND COMMUNICATION TASK
            second_comm = self.check_second_communication()
            if second_comm == "ATTENTION":
                event = 2
            elif second_comm == "HELP":
                event = 3

        # CONDITION FOR AREA VALUES UNDER THRESHOLD WHILE CHANGING FOCUS
        elif last_area < win_mean - thr * win_mean:
            event = -1  # Possible change-focus event
        else:
            event = 0
            if self.key_mean:
                self.key_mean = None  # Resetting key value
                self.start_time_event = 0  # resetting start event time
                self.th1.run()

        if self.events.size > 0 and self.events[-1] != event:  # print event when changes
            print("event: {}".format(event))
        self.events = np.append(self.events, event)

        # FIRST AREA VALUE UNDER THRESHOLD
        action = False  # Boolean checking if the user has done the focus task
        if self.key_mean is None and self.event_confirmation():
            self.key_mean = self.means[-3] - thr * win_mean
            action = True
            self.start_time_event = time.time()
        return thr, n, action, second_comm

    def event_confirmation(self):
        # Check if it is a change of focus
        win_len = 4
        win = self.events[-win_len:]
        if np.sum(win) == -win_len:
            self.events[-win_len:] = 1
            return True
        else:
            return False

    def check_second_communication(self):
        seconds_attention = 3
        seconds_help = 6
        delta_time = time.time() - self.start_time_event
        if delta_time > seconds_help:
            return "HELP"
        elif delta_time > seconds_attention:
            return "ATTENTION"
        else:
            return None


class SaveToFile:
    def save(self, save_path, areas, means, events):
        np.savetxt(save_path + 'areas.txt', areas, fmt='%d')
        np.savetxt(save_path + 'means.txt', means, fmt='%.2f')
        np.savetxt(save_path + 'events.txt', events, fmt='%d')


class ObjectActionThread(threading.Thread):
    def __init__(self, ID, response=None, pin=27):
        super().__init__(daemon=True)
        self.ID = ID
        self.device = platform.node()
        self.response = response
        self.pin = pin

    def run(self):
        if self.response is None:
            if self.device == 'raspberrypi':
                import RPi.GPIO as GPIO
                GPIO.setmode(GPIO.BCM)
                GPIO.setwarnings(False)
                GPIO.setup(self.pin, GPIO.OUT)
                GPIO.output(self.pin, GPIO.HIGH)
                print("light ON")
                time.sleep(0.5)
                GPIO.output(self.pin, GPIO.LOW)
                print("light OFF")
            else:
                try:
                    from winsound import Beep
                    Beep(frequency=2500, duration=500)
                except ImportError:
                    from os import system
                    system('spd-say ""')
        else:
            pygame.mixer.init()
            if self.response == "NO":
                no = pygame.mixer.Sound("./Yes-No Graph Files/no-1.wav")
                no.play()
            elif self.response == "YES":
                yes = pygame.mixer.Sound("./Yes-No Graph Files/oh-yeah-2.wav")
                yes.play()
            elif self.response == "NOT_SURE":
                boh = pygame.mixer.Sound("./Yes-No Graph Files/what-2.wav")
                boh.play()
            elif self.response == "ATTENTION":
                att = pygame.mixer.Sound("./Yes-No Graph Files/AyHee.wav")
                att.play()
            elif self.response == "HELP":
                help = pygame.mixer.Sound("./Yes-No Graph Files/INeedSomeHelp.wav")
                help.play()

class FocusPlot:
    def __init__(self, win):
        self.win = win
        self.p = self.win.addPlot(title="Areas")
        self.p.addLegend()
        self.p.showGrid(y=True)
        self.c1 = self.p.plot(pen='y', name="area", symbol='s')
        self.c2 = self.p.plot(pen='r', name="mean")
        self.c3 = self.p.plot(pen='g', name="mean +- 0.15* mean")
        self.c4 = self.p.plot(pen='g')

    def setData(self, indexes, areas, means, thr):
        self.c1.setData(indexes, areas)
        self.c2.setData(indexes, means)
        self.c3.setData(indexes, means + thr * means)
        self.c4.setData(indexes, means - thr * means)


class GenerateYesNoGraph(threading.Thread):
    def __init__(self, x_dim, y_dim, t_width, seconds_per_side):
        super().__init__()
        pygame.init()
        self.size = x_dim, y_dim
        self.width = t_width
        self.pause = int(seconds_per_side * 1000 * 2 / x_dim)
        self.screen = pygame.display.set_mode(self.size)
        pygame.display.set_caption('Yes-No Slider')
        self.im_surface = pygame.image.load("./Yes-No Graph Files/yes-no.png").convert()
        self.im_surface = pygame.transform.scale(self.im_surface, self.size)
        self.rect = self.im_surface.get_rect()
        self.x_coord = 0
        self.run_event = threading.Event()
        self.run_event.set()
        self.terminated = False

    def update_x_coord(self):
        if self.x_coord == self.size[0]:
            self.x_coord = 0
        else:
            self.x_coord += 1

    def run(self):
        while self.run_event.is_set():
            self.update_x_coord()
            self.screen.blit(self.im_surface, self.rect)
            pygame.draw.line(self.screen, (255, 255, 255), (self.x_coord, 0), (self.x_coord, self.size[1]), self.width)
            pygame.time.wait(self.pause)
            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_q:
                        self.terminated = True
                        self.run_event.clear()
            pygame.display.flip()


if __name__ == "__main__":
    yesno = GenerateYesNoGraph(500, 500, 5, 3)
    yesno.start()
    cam_num = 0
    cap1 = CameraAcquisition(cam_num)
    _, frame = cap1.getFrame()
    i = np.array([0])
    proc = ImageProcessing(frame)
    focId = FocusIdentifier()
    delta_frame = 10  # every delta frames adjusting focus detection based on FPS
    fps = 20
    n = -1
    win = pg.GraphicsWindow()
    fp = FocusPlot(win)
    a = (proc.frame.shape[-2], proc.frame.shape[0])
    save_path = './Recordings/'
    out = cap1.save_video_frames(save_path + 'output.avi', 30, (proc.frame.shape[-2], proc.frame.shape[0]))
    while True:
        ret1, frame1 = cap1.getFrame()
        if not ret1:
            continue
        _, pupil, _, area = proc.get_pupil(frame1)
        focId.FilteredArea(area, max_area=3000)

        # Calculate FPS
        if i.size > 1 and i[-1] % delta_frame == 0:
            fps = cap1.FPS_COUNTER(delta_frame)
            print("fps: {} || N: {}".format(fps, n))

        thr, offset, n = focId.IdentifyFocus(fps)

        fp.setData(i, focId.areas, focId.means, thr)
        fp.p.setLimits(xMin=max(0, i[-1] - 80), xMax=i[-1] + 30)
        # fp.p.setTitle('STD: {}'.format(mod_std))

        QtGui.QApplication.processEvents()

        pretty = proc.getPrettyImage(pupil)
        out.write(pretty)
        cv2.imshow("prova", pretty)
        if cv2.waitKey(10) & 0xFF == ord('q'):
            print("Stopping")
            cap1.stopAcquiring()
            cv2.destroyAllWindows()
            yesno.run_event.clear()
            sf = SaveToFile()
            sf.save(save_path, focId.areas, focId.means, focId.events)
            print("Saved")
            out.release()
            print("Correctly released")
            exit(1)
        i = np.append(i, i[-1] + 1)
