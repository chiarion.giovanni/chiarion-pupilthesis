from flask import Flask, render_template, Response
import cv2
from CameraAcquisition import CameraAcquisition
from ImageProcessing import ImageProcessing

app = Flask(__name__)
_, frame = CameraAcquisition(1).getFrame()
proc = ImageProcessing(frame)


@app.route('/')
def index():
    return render_template('index.html')


def gen(camera):
    while True:
        ret, frame = camera.getFrame()
        _, pupil, _, _ = proc.get_pupil(frame)
        pretty = proc.getPrettyImage(pupil)
        both = cv2.imencode('.jpg', pretty)[1].tobytes()
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + both + b'\r\n')


@app.route('/video_feed')
def video_feed():
    return Response(gen(CameraAcquisition(1)),
                    mimetype='multipart/x-mixed-replace; boundary=frame')


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)
