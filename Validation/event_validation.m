clear
close all
clc


load('areas.txt')
load('events.txt')
load('means.txt')

frames = 1:length(areas);
yes_no_index = events==1;
might_index = events==-1;
attention_index = events==2;
help_index = events ==3;
lower_line = means-0.17*means;
j=1;
intersection_index=2; %inizializzazione
for i=2:length(frames)
   if yes_no_index(i)==1 && yes_no_index(i-1)==0
       intersection_index(j) = i;
       j=j+1;
   end       
end
intersection_index = intersection_index - 1;


plot(frames,areas)
hold on
plot(frames(yes_no_index),areas(yes_no_index),'go')
% plot(frames(might_index),areas(might_index),'o','MarkerFaceColor',[1 0 1])
plot(frames(attention_index),areas(attention_index),'o','MarkerEdgeColor',[255 165 0]/255)
plot(frames(help_index),areas(help_index),'ro')
title('Event Detection','fontsize',15)
xlabel('Numero di frames','fontsize',16,'FontWeight','bold')
ylabel('Area in pixel della pupilla segmentata','fontsize',16,'FontWeight','bold')
xlim([0,length(frames)])
if intersection_index~=1
    plot(frames(intersection_index),areas(intersection_index),'k+','MarkerSize',12,'LineWidth',2)
    % legend('Acquisizione','Soglia','Yes/No','Possibile evento','Richiesta di attenzione','Richiesta di aiuto')
    legend({'Acquisizione','Yes/No','Richiesta di attenzione','Richiesta di aiuto','Soglia'},'fontsize',15,'Orientation','horizontal','Location','north')
else
    legend({'Acquisizione','Yes/No','Richiesta di attenzione','Richiesta di aiuto'},'fontsize',15,'Orientation','horizontal','Location','north')
end

%% NUMERIC VALIDATION
number_of_events = length(frames);
number_of_0 = sum(events==0);
number_of_1 = sum(yes_no_index);
number_of_m1 = sum(might_index);
number_of_2 = sum(attention_index);
number_of_3 = sum(help_index);
