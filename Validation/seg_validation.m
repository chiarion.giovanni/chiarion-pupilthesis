clear all
close all
clc

% ------------------------------------------------------------------------
% -------------------------- FRAME EXTRACTION ----------------------------
% ------------------------------------------------------------------------

v_norm = VideoReader('output.avi');
v_cont = VideoReader('output_pupil_mask.avi');
mkdir 'Frames'
cd 'Frames'
i=0;
while hasFrame(v_norm)
    fr_norm=readFrame(v_norm);
    fr_cont=readFrame(v_cont);
    if mod(i,50)==0
        imwrite(fr_norm,[num2str(i),'.png']);
        imwrite(fr_cont,[num2str(i),'_cont.png']);
    end
    i=i+1;
    disp(['Frame num: ',num2str(i)])
end
cd '..'
disp('Delete bad images, then press a button to continue')
pause

%% ------------------------------------------------------------------------
% -------------------------- MANUAL SEGMENTATION -------------------------
% ------------------------------------------------------------------------

cd 'Frames'
dir_content = dir('./*.png');
num_images = length(dir_content);
j=1;
for i=1:2:num_images
    filename_norm = dir_content(i).name;
    filename_pupil = dir_content(i+1).name;
    image_norm = imread(filename_norm);
    image_pupil = imbinarize(rgb2gray(imread(filename_pupil)));
    manual_mask = roipoly(image_norm);
    imwrite(manual_mask,[filename_norm(1:end-4),'_manual.png'])
    dice_values(j) = dice(image_pupil,manual_mask);
    imshowpair(image_pupil,manual_mask)
    title(['Image: ',filename_norm,' dice: ',num2str(dice_values(j))])
    disp('Press Pause to continue')
    pause
    j=j+1;
end
save('dice_values.txt','dice_values','-ascii')
cd '..'

%%

cd 'Frames'
dir_content = dir('./*.png');
num_images = length(dir_content);
j=1;
for i=1:3:num_images
    filename_norm = dir_content(i).name;
    filename_pupil = [filename_norm(1:end-4),'_cont.png'];
    filename_manual = [filename_norm(1:end-4),'_manual.png'];
    image_pupil = imbinarize(rgb2gray(imread(filename_pupil)));
    manual_mask = imread(filename_manual);
    dice_values(j) = dice(image_pupil,manual_mask);
    j=j+1;
end
mean = mean(dice_values);
save('dice_values.txt','dice_values','-ascii')
save('mean.txt','mean','-ascii')
cd '..'







