import cv2
import numpy as np
from CameraAcquisition import CameraAcquisition

cam_num = 1


class ImageProcessing:
    def __init__(self, frame):
        self.frame = self.imCrop(frame)
        self.min_area = self.get_pupil_min_area()
        self.area = 0
        self.ecc = 0
        self.pupil = 0

    def get_pupil(self, frame):
        self.frame = self.imCrop(frame)
        gray_frame = cv2.cvtColor(self.frame, cv2.COLOR_BGR2GRAY)
        filtered_frame = cv2.subtract(cv2.blur(gray_frame, (20, 20)), gray_frame)
        otsu_thresh, _ = cv2.threshold(filtered_frame[filtered_frame > 0], 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
        _, binarized_frame = cv2.threshold(filtered_frame, otsu_thresh, 255, cv2.THRESH_BINARY)
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (5, 5))
        dilated_frame = cv2.dilate(binarized_frame, kernel)
        filled_frame = self.get_filled_frame(dilated_frame)
        # opened_frame = self.BWAreaOpen(filled_frame, self.min_area)
        # ecc, pupil = self.select_frame_by_Eccentricity(opened_frame, 0.3)
        self.ecc, self.pupil, self.area = self.SelectPupil(filled_frame, self.min_area, 0.30)
        return self.frame, self.pupil, self.ecc, self.area

    def get_pupil_min_area(self):
        pupil_min_radius = (min(self.frame.shape[0:1]) / 19.0) / 2
        return int(round(pupil_min_radius ** 2 * np.pi))

    def get_filled_frame(self, frame):
        im_floodfill = frame.copy()
        h, w = frame.shape[:2]
        mask = np.zeros((h + 2, w + 2), np.uint8)
        cv2.floodFill(im_floodfill, mask, (0, 0), 255)
        inverted = cv2.bitwise_not(im_floodfill)
        return frame | inverted

    def BWAreaOpen(self, frame, min_area):
        nb_components, output, stats, centroids = cv2.connectedComponentsWithStats(frame, connectivity=8)
        im = np.zeros(output.shape)
        for i in range(1, nb_components):
            if stats[i, 4] >= min_area:
                im[output == i] = 255
        return cv2.convertScaleAbs(im)

    def select_frame_by_Eccentricity(self, frame, delta_range):
        nb_components, output, stats, centroids = cv2.connectedComponentsWithStats(frame, connectivity=8)
        im = np.zeros(output.shape)
        ecc_list = list(())
        for i in range(1, nb_components):
            im2 = np.zeros(output.shape)
            im2[output == i] = 255
            cnt = cv2.findContours(cv2.convertScaleAbs(im2), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
            perimeter = cnt[1][0].shape[0]  # OPENCV 4 perimeter = cnt[0][0].shape[0]
            area = np.sum(im2) / 255
            ecc = (4 * np.pi * area) / perimeter ** 2
            if abs(ecc - 1) < delta_range:  # 1 - delta_range < ecc < 1 + delta_range:
                ecc_list.append((i, ecc))
        ecc_list = sorted(ecc_list, key=lambda x: abs(x[1] - 1))
        if len(ecc_list) == 0:
            return 1, cv2.convertScaleAbs(im)
        eccentricity = ecc_list[0][1]
        im[output == ecc_list[0][0]] = 255
        return eccentricity, cv2.convertScaleAbs(im)

    def SelectPupil(self, frame, min_area, delta_range):
        nb_components, output, stats, centroids = cv2.connectedComponentsWithStats(frame, connectivity=8)
        parameter_list = list(())
        im = np.zeros(output.shape)
        for i in range(1, nb_components):
            area = stats[i, 4]
            # BWAreaOpen
            if area >= min_area:
                im[output == i] = 255
                cnt = cv2.findContours(cv2.convertScaleAbs(im), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
                perimeter = cnt[1][0].shape[0]  # OPENCV 4 perimeter = cnt[0][0].shape[0]
                ecc = (4 * np.pi * area) / perimeter ** 2
                # Eccentricity Selection
                if abs(ecc - 1) < delta_range:  # 1 - delta_range < ecc < 1 + delta_range:
                    parameter_list.append((i, ecc, area))
                im = np.zeros(output.shape)

        parameter_list = sorted(parameter_list, key=lambda x: abs(x[1] - 1))
        if len(parameter_list) == 0:
            return 0, cv2.convertScaleAbs(im), 0
        im[output == parameter_list[0][0]] = 255
        eccentricity = parameter_list[0][1]
        area = parameter_list[0][2]
        return eccentricity, cv2.convertScaleAbs(im), area

    def getPrettyImage(self, pupil):
        cnt = cv2.findContours(cv2.convertScaleAbs(pupil), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
        return cv2.drawContours(self.frame, cnt[1], 0, [0, 255, 0], 3)

    def imCrop(self, frame):
        dim = np.shape(frame)
        new_frame = frame[0:dim[0], int(round(dim[1]) / 2):dim[1]]  # y1:y2,x1:x2
        return new_frame


if __name__ == "__main__":
    cap1 = CameraAcquisition(cam_num)
    _, frame1 = cap1.getFrame()
    proc = ImageProcessing(frame1)

    # cap2 = CameraAcquisition(2)
    i = 0
    while True:
        ret1, frame1 = cap1.getFrame()
        # ret2, frame2 = cap2.getFrame()
        _, pupil, _, _ = proc.get_pupil(frame1)
        pretty = proc.getPrettyImage(pupil)
        # im2 = ImageProcessing().get_pupil(frame2)
        # both1 = np.concatenate((frame1[:, :, 1], im1), axis=1)
        # both2 = np.concatenate((frame2[:, :, 1], im2), axis=1)
        # both = np.concatenate((both1, both2))
        cv2.imshow("prova", pretty)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            print("Stopping")
            cap1.stopAcquiring()
            cv2.destroyAllWindows()
        # time.sleep(0.5)
        FPS = cap1.FPS_COUNTER(i)
        print("frame {}, {} FPS".format(i, FPS))
        # print(f"SHAPE:{im1.shape}")
        i += 1
        print("frame {}, {} FPS".format(i, FPS))
        # print(f"SHAPE:{im1.shape}")
        i += 1
