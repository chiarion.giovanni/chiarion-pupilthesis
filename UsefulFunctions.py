import platform


class UsefulFunctions:
    def __init__(self):
        self.device = platform.node()
        if self.device == "raspberrypi":
            import RPi.GPIO as GPIO
            GPIO.setmode(GPIO.BCM)
            GPIO.setwarnings(False)
            self.change_mode_pin = 17
            GPIO.setup(self.change_mode_pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)

    def changing_mode_condition(self):
        if self.device == "raspberrypi":
            import RPi.GPIO as GPIO
            command = GPIO.input(self.change_mode_pin)
            if command == False:
                print("BUTTON PRESSED")
                return True
            else:
                return False
        return False
