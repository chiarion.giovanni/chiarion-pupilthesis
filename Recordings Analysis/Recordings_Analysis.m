clear all
close all
clc

load('areas.txt')
load('events.txt')
load('means.txt')

vid = VideoReader('output.avi');
frame_num=1;
figure('units','normalized','outerposition',[0 0 1 1])
while hasFrame(vid)
    frame=readFrame(vid);
    subplot(121)
    imshow(frame)
    title(['Frame number: ',num2str(frame_num-1)])
    subplot(122)
    plot(1:frame_num,areas(1:frame_num),'b')
    title(['Event: ',num2str(events(frame_num))])
    hold on
    if events(frame_num)==1  
        plot(frame_num,areas(frame_num),'k*')
    end
    plot(1:frame_num,means(1:frame_num),'ro')
    plot(1:frame_num,means(1:frame_num)+0.15*means(1:frame_num),'g+')
    plot(1:frame_num,means(1:frame_num)-0.15*means(1:frame_num),'g+')
    hold off
    legend('Area','Mean','Mean + Std','Mean - Std')
    frame_num=frame_num + 1;
    pause(.1)
end