# coding=utf-8
import paho.mqtt.client as PahoMQTT
import threading
import time


class Publisher(object):
    def __init__(self, clientID):
        self.clientID = clientID
        self._paho_mqtt = PahoMQTT.Client(clientID, True)  # durable connection
        self._paho_mqtt.on_connect = self.myOnConnect
        self.topic = "control_bot"
        self.messageBroker = "iot.eclipse.org"

    def start(self):
        # manage connection to broker
        self._paho_mqtt.connect(self.messageBroker, 1883)
        self._paho_mqtt.loop_start()
        # subscribe for a topic
        self._paho_mqtt.subscribe(self.topic, 2)  # QoS 2
        print("subscribed " + self.topic)

    def stop(self):
        self._paho_mqtt.unsubscribe(self.topic)
        self._paho_mqtt.loop_stop()
        self._paho_mqtt.disconnect()

    def myOnConnect(self, paho_mqtt, userdata, flags, rc):
        print("Connected to %s with result code: %d" % (self.messageBroker, rc))

    def myPublish(self, topic, message):
        # publish a message with a certain topic
        self._paho_mqtt.publish(topic, message, 2)


class PublisherThread(threading.Thread):
    def __init__(self):
        super().__init__()
        self.publisher = Publisher("pub_1")
        self.publisher.start()

    def run(self):
        while True:
            time.sleep(0.3)
            pass
