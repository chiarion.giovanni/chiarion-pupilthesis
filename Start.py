from CameraAcquisition import CameraAcquisition
from ImageProcessing import ImageProcessing
from FocusIdentifier import FocusIdentifier
from FocusIdentifier import FocusPlot
from FocusIdentifier import SaveToFile
from FocusIdentifier import GenerateYesNoGraph
from FocusIdentifier import ObjectActionThread
import cv2
import numpy as np
import pyqtgraph as pg
from pyqtgraph.Qt import QtGui
from datetime import datetime
import os
from UsefulFunctions import UsefulFunctions
from publisher_mqtt import PublisherThread
import json

# -----------------------    PARAMETERS    ------------------------------------------

cam_num = 0
fps = 20  # FPS initialization
delta_frame = 20  # every delta frames adjusting focus detection based on FPS
show_area_plot = False  # True if you want to see area plot for debugging; False if not
show_cam = False  # True if you want to see the camera frames; False if not
show_yesno = True
x_dim = 640
y_dim = 480
t_width = 5
seconds_per_side = 3
video_filename = 'output'
recording_directory_path = './Recordings/'

# --------------------------    CODE     -------------------------------------------

cap_calibration = CameraAcquisition(cam_num)
_, frame_calibration = cap_calibration.getFrame()
proc_calibration = ImageProcessing(frame_calibration)
focId_calibration = FocusIdentifier()

# ------ CALIBRATION MODE ----------------
change_mode = False
while not change_mode:
    ret1, frame1 = cap_calibration.getFrame()
    if not ret1:
        continue
    crop_frame, pupil, _, area = proc_calibration.get_pupil(frame1)
    focId_calibration.FilteredArea(area, max_area=4000)
    contoured_calibration = proc_calibration.getPrettyImage(pupil)
    cv2.imshow("Camera Acquisition", contoured_calibration)
    change_mode = UsefulFunctions().changing_mode_condition()
    if cv2.waitKey(10) & 0xFF == ord('n'):
        print("Going to communication mode")
        break

print("Stopping Camera Acquisition")
cap_calibration.stopAcquiring()
cv2.destroyAllWindows()

# ------ COMMUNICATION MODE ----------------
if not os.path.isdir(recording_directory_path):
    os.mkdir(recording_directory_path)
time_folder_path = datetime.now().strftime('%Y-%m-%d %H_%M_%S') + '/'
os.mkdir(recording_directory_path + time_folder_path)
recording_directory_path += time_folder_path

# -------- STARTING MQTT PUBLISHER -------------

pub_th = PublisherThread()
pub_th.daemon = True
pub_th.start()

# ----------------------------------------------
i = np.array([0])
cap1 = CameraAcquisition(cam_num)
ret, frame = cap1.getFrame()
while not ret:
    ret, frame = cap1.getFrame()
proc = ImageProcessing(frame)
focId = FocusIdentifier()
n = -1
if show_area_plot is True:
    win = pg.GraphicsWindow()
    fp = FocusPlot(win)
a = (proc.frame.shape[-2], proc.frame.shape[0])
out_normal = cap1.save_video_frames(recording_directory_path + video_filename + '.avi', 30,
                                    (proc.frame.shape[-2], proc.frame.shape[0]))
out_pupil = cap1.save_video_frames(recording_directory_path + video_filename + '_pupil_mask.avi', 30,
                                   (proc.frame.shape[-2], proc.frame.shape[0]))
# out_pretty = cap1.save_video_frames(recording_directory_path + video_filename+'_contoured.avi', 30, (proc.frame.shape[-2], proc.frame.shape[0]))

second_comm_counter = 0
if show_yesno is True:
    yes_no = GenerateYesNoGraph(x_dim=x_dim, y_dim=y_dim, t_width=t_width, seconds_per_side=seconds_per_side)
    yes_no.daemon = True
    yes_no.start()
while True:
    ret1, frame1 = cap1.getFrame()
    if not ret1:
        continue
    crop_frame, pupil, _, area = proc.get_pupil(frame1)
    focId.FilteredArea(area, max_area=4000)

    # Calculate FPS
    if i.size > 1 and i[-1] % delta_frame == 0:
        fps = cap1.FPS_COUNTER(delta_frame)
        # DEBUG FPS
        # print("fps: {} || N: {}".format(fps, n))

    thr, n, action, second_comm = focId.IdentifyFocus(fps, thr=0.17)

    # -------------------- SECONDARY COMMUNICATION -------------------------------

    if second_comm is not None:
        if second_comm_counter == 0 and second_comm == "ATTENTION":
            ObjectActionThread('thread_yes_no', response=second_comm).start()
            second_comm_counter += 1
            print("HEY! {}".format(second_comm))
        elif second_comm_counter == 1 and second_comm == "HELP":
            ObjectActionThread('thread_yes_no', response=second_comm).start()
            message = dict()
            message["command"] = "send_alarm"
            pub_th.publisher.myPublish("control_bot", json.dumps(
                                        message))  # PUBLISH TO SEND ALARM IF THERE IS A MEDIC SAVED IN JSON FILE
            second_comm_counter += 1
            print("HELP! {}".format(second_comm))

    # -------------------- PRIMARY COMMUNICATION -------------------------------

    if action and show_yesno:
        second_comm_counter = 0
        if 0 <= yes_no.x_coord <= int(x_dim * 0.0583) or int(x_dim * 0.456) <= yes_no.x_coord <= int(
                x_dim * 0.543) or yes_no.x_coord >= int(x_dim * 0.9406):
            ObjectActionThread('thread_yes_no', response="NOT_SURE").start()
            print("NOT SURE OF ANSWER")
        elif int(x_dim * 0.0583) < yes_no.x_coord < int(x_dim * 0.456):
            ObjectActionThread('thread_yes_no', response="YES").start()
            print("YES")
        else:
            ObjectActionThread('thread_yes_no', response="NO").start()
            print("NO")

    if show_area_plot is True:
        fp.setData(indexes=i, areas=focId.areas, means=focId.means, thr=thr)
        fp.p.setLimits(xMin=max(0, i[-1] - 80), xMax=i[-1] + 30)
        QtGui.QApplication.processEvents()

    out_normal.write(crop_frame)
    contoured = proc.getPrettyImage(pupil)
    out_pupil.write(cv2.cvtColor(pupil, cv2.COLOR_GRAY2BGR))
    # out_pretty.write(contoured)

    if show_cam is True:
        cv2.imshow("Camera Acquisition", contoured)

    # SAVING DATA FILES EVERY STEP OF THE LOOP
    SaveToFile().save(recording_directory_path, focId.areas, focId.means, focId.events)

    if yes_no.terminated is True:
        print("Stopping")
        cap1.stopAcquiring()
        cv2.destroyAllWindows()
        if show_yesno:
            yes_no.run_event.clear()
        sf = SaveToFile()
        sf.save(recording_directory_path, focId.areas, focId.means, focId.events)
        print("All files are correclty saved")
        out_normal.release()
        out_pupil.release()
        # out_pretty.release()
        exit(1)
    i = np.append(i, i[-1] + 1)
