import cv2
import time


class CameraAcquisition:
    def __init__(self, cam_num):
        self.camNum = cam_num
        self.cap = self._startCamera()
        self.cap.set(cv2.CAP_PROP_FPS, 25)
        self.start_time = time.time()

    def _startCamera(self):
        return cv2.VideoCapture(self.camNum)

    def getFrame(self, frame_num=1):
        if frame_num != 1:
            self.cap.set(cv2.CAP_PROP_POS_FRAMES, frame_num - 1)
        return self.cap.read()

    def stopAcquiring(self):
        self.cap.release()

    def FPS_COUNTER(self, frame_num):
        now = time.time()
        delta = now - self.start_time
        self.start_time = time.time()
        return round((frame_num + 1) / delta, 1)

    def save_video_frames(self, file_name, fps, frame_size):
        fourcc = cv2.VideoWriter_fourcc(*'MP4V')
        return cv2.VideoWriter(file_name, fourcc, fps, frame_size)


if __name__ == "__main__":
    cap = CameraAcquisition(0)
    i = 1
    # ret, frame = cap.getFrame()
    while True:
        ret, frame = cap.getFrame()
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        blur = cv2.subtract(cv2.blur(gray, (20, 20)), gray)
        level, _ = cv2.threshold(blur[blur > 0], 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
        _, otsu = cv2.threshold(blur, level, 255, cv2.THRESH_BINARY)
        cv2.imshow('pc_camera', otsu)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            print("Stopping")
            cap.stopAcquiring()
            cv2.destroyAllWindows()
            break
        i += 1
