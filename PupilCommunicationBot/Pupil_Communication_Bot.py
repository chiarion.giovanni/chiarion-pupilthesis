from TelegramBot import TelegramBot
import time
import threading
import paho.mqtt.client as PahoMQTT
import json


class BotSubscriber(object):
    def __init__(self, clientID, bot_thread):
        self.bot_thread = bot_thread
        self.clientID = clientID
        # create an instance of paho.mqtt.client
        self._paho_mqtt = PahoMQTT.Client(clientID, False)  # durable connection
        # register the callback
        self._paho_mqtt.on_connect = self.myOnConnect
        self._paho_mqtt.on_message = self.myOnMessageReceived
        self.topic = "control_bot"
        self.messageBroker = "iot.eclipse.org"

    def start(self):
        # manage connection to broker
        self._paho_mqtt.connect(self.messageBroker, 1883)
        self._paho_mqtt.loop_start()
        # subscribe for a topic
        self._paho_mqtt.subscribe(self.topic, 2)  # QoS 2
        print("subscribed " + self.topic)

    def stop(self):
        self._paho_mqtt.unsubscribe(self.topic)
        self._paho_mqtt.loop_stop()
        self._paho_mqtt.disconnect()

    def myOnConnect(self, paho_mqtt, userdata, flags, rc):
        print("Connected to %s with result code: %d" % (self.messageBroker, rc))

    def myOnMessageReceived(self, paho_mqtt, userdata, msg):
        command = json.loads(msg.payload).get("command")
        if command == "send_alarm":
            self.bot_thread.pupil_bot.send_alarm_if_valid()
        else:
            print("Comando {} non riconosciuto!".format(command))


class PupilCommunicationBot:
    def __init__(self):
        self.bot = TelegramBot()

    def start_routine(self, user_id):
        print("Started initial routine by {}\n".format(user_id))
        self.bot.send_message(user_id, "Benvenuto, inserisci l'ID del paziente")
        while self.check_correct(user_id, "patient", "ID") is False:
            time.sleep(2)
        self.bot.send_message(user_id, "Paziente identificato, inserisci il tuo ID")
        while self.check_correct(user_id, "medic", "ID") is False:
            time.sleep(2)
        self.bot.send_message(user_id, "Medico identificato, inserisci la tua password")
        while self.check_correct(user_id, "medic", "password") is False:
            time.sleep(2)
        self.bot.send_message(user_id,
                              "Password riconosciuta! Benvenuto {}, il tuo ID è stato aggiunto e associato al paziente {}".format(
                                  self.bot.get_medic_full_name(), self.bot.get_patient_full_name()))
        time.sleep(3)
        keys = ["Controlla stato", "Attiva notifiche"]
        keyboard = self.bot.build_keyboard(keys)
        self.bot.send_message(user_id, "Seleziona una possibile opzione", keyboard)
        self.bot.save_medic_user_id(user_id)

    def handle_alert(self, user_id, message):
        if user_id == self.bot.get_medic_chat_ID():
            if message == 'Controlla stato':
                print("Checked State\n")
                self.bot.send_message(user_id, "Attualmente hai le notifiche su {}".format(self.bot.check_alarm_state()))
            elif message == "Disattiva notifiche":
                print("Notifications deactivated\n")
                self.bot.save_medic_alarm_state("OFF")
                self.bot.send_message(user_id, "Notifiche disattivate!")
            elif message == "Attiva notifiche":
                print("Notifications activated\n")
                self.bot.save_medic_alarm_state("ON")
                self.bot.send_message(user_id, "Notifiche attivate!")
            elif message == "\start":
                pass
            else:
                print("Unrecognised Command\n")
                self.bot.send_message(user_id, "Comando non riconosciuto")
            state = self.bot.check_alarm_state()
            if state == "ON":
                keys = ["Controlla stato", "Disattiva notifiche"]
            else:
                keys = ["Controlla stato", "Attiva notifiche"]
            keyboard = self.bot.build_keyboard(keys)
            time.sleep(1)
            self.bot.send_message(user_id, "Seleziona una possibile opzione", keyboard)
        else:
            self.bot.send_message(user_id, 'Non hai accesso a questi comandi, esegui l''autenticazione')
            self.start_routine(user_id)

    def check_correct(self, user_id, target, field):
        updates = self.bot.get_updates()
        if len(updates.get("result")) > 0:
            if self.bot.get_message() != "/start":
                answ = self.bot.get_message()
                print("Entered: {} as {} for {}\n".format(answ, field, target))
            else:
                return False
            if self.bot.load_info().get(target).get(field) == answ:
                print("Correct {}\n".format(field))
                return True
            else:
                self.bot.send_message(user_id, "ID o password non coincidente, riprova!".format(field, target))
                print("Uncorrect {} for {}, retrying\n".format(field, target))
                return False
        else:
            return False

    def check_valid(self):
        if self.bot.check_paired_medic() is True and self.bot.check_alarm_state() == "ON":
            return True
        else:
            return False

    def send_alarm_if_valid(self):
        if self.check_valid():
            medic_chat_ID = self.bot.get_medic_chat_ID()
            self.bot.send_message(medic_chat_ID,
                                  "Il tuo paziente, {}, stà chiedendo aiuto!!".format(self.bot.get_patient_full_name()))
        else:
            return


class BotThread(threading.Thread):
    def __init__(self, pupil_bot):
        super().__init__()
        self.pupil_bot = pupil_bot

    def run(self):
        while True:
            updates = self.pupil_bot.bot.get_updates()
            if len(updates.get("result")) > 0:
                user_id = self.pupil_bot.bot.get_user_id()
                message = self.pupil_bot.bot.get_message()
                if self.pupil_bot.bot.check_paired_medic() is False:
                    if message == "/start":
                        self.pupil_bot.start_routine(user_id)
                    else:
                        self.pupil_bot.bot.send_message(user_id, "Devi prima eseguire la fase di registrazione")
                        self.pupil_bot.start_routine(user_id)
                else:
                    self.pupil_bot.handle_alert(user_id, message)
            time.sleep(0.5)


if __name__ == "__main__":
    pupil_bot = PupilCommunicationBot()
    th = BotThread(pupil_bot)
    th.daemon = True
    bot_sub = BotSubscriber("bot_sub", th)
    bot_sub.start()
    th.run()
