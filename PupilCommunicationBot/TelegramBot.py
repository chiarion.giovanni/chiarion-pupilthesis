import requests
import json


class TelegramBot:
    def __init__(self):
        token = "836116298:AAHBLRckcvJxeCjEJCJDdoJ0PMJ_cIOBIsI"
        self.url = "https://api.telegram.org/bot{}/".format(token)
        self.last_update_id = None
        self.updates = list()
        self.last_message = ""

    def load_info(self):
        # try:
        #     with open("./PupilCommunicationBot/medic_patient_info.json", 'r') as f:
        #         file = f.read()
        # except FileNotFoundError:
        with open("./medic_patient_info.json", 'r') as f:
            file = f.read()
        return json.loads(file)

    def get_last_update_id(self):
        updates_ids = []
        for update in self.updates.get('result'):
            updates_ids.append(int(update["update_id"]))
        return max(updates_ids)

    def get_updates(self):
        if self.last_update_id:
            self.updates = requests.get(
                self.url + "GetUpdates?offset={}&timeout=1&limit=1".format(self.last_update_id + 1)).json()
        else:
            self.updates = requests.get(self.url + "GetUpdates").json()
        if len(self.updates.get("result")) > 0:
            self.last_update_id = self.get_last_update_id()
        return self.updates

    def send_message(self, chat_id, message, reply_markup=None):
        if reply_markup:
            requests.post(
                self.url + 'sendMessage?chat_id={}&text={}&reply_markup={}'.format(chat_id, message, reply_markup))
        else:
            requests.post(self.url + 'sendMessage?chat_id={}&text={}'.format(chat_id, message))

    def build_keyboard(self, keys):
        keyboard = [[key] for key in keys]
        reply_markup = {"keyboard": keyboard, "one_time_keyboard": True}
        return json.dumps(reply_markup)

    def check_paired_medic(self):
        """
        Controls if there is a medic paired with the patient in the JSON file
        :return: TRUE or FALSE
        """
        info = self.load_info()
        medic_id = info.get("medic").get("chat_id")
        if medic_id == "None":
            return False
        else:
            return True

    def get_user_id(self):
        return self.updates.get("result")[0]["message"]["from"]["id"]

    def get_message(self):
        return self.updates.get("result")[0]["message"]["text"]

    def get_medic_chat_ID(self):
        info = self.load_info()
        return info["medic"]["chat_id"]

    def save_medic_user_id(self, ID):
        info = self.load_info()
        info["medic"]["chat_id"] = ID
        with open("./medic_patient_info.json", 'w') as f:
            f.write(json.dumps(info, indent=1))

    def check_alarm_state(self):
        info = self.load_info()
        return info["medic"]["alarm_state"]

    def save_medic_alarm_state(self, new_alarm_state):
        info = self.load_info()
        info["medic"]["alarm_state"] = new_alarm_state
        with open("./medic_patient_info.json", 'w') as f:
            f.write(json.dumps(info, indent=1))

    def get_patient_full_name(self):
        info = self.load_info()
        return info["patient"]["name"] + " " + info["patient"]["surname"]

    def get_medic_full_name(self):
        info = self.load_info()
        return info["medic"]["name"] + " " + info["medic"]["surname"]
